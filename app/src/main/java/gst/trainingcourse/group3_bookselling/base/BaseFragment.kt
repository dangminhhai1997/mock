package gst.trainingcourse.group3_bookselling.base

import android.view.Gravity
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.databinding.ToastWarningBinding
import gst.trainingcourse.group3_bookselling.view.home.HomeActivity

abstract class BaseFragment : Fragment() {
    protected var clickable = true

    protected val msgObserver = Observer<String> { msg ->
        if (msg.isNotEmpty()) {
            showToast(msg)
        }
        hideLoadingView()
        clickable = true
    }

    fun setName(name: String) {
        (activity as? HomeActivity)?.setToolBarTitle(name)
    }

    private fun showToast(msg: String) {
        val customToastView = ToastWarningBinding.inflate(layoutInflater)
        customToastView.toastText.text = msg
        context?.run {
            Toast(context).apply {
                view = customToastView.root
                duration = Toast.LENGTH_SHORT
                setGravity(Gravity.BOTTOM, 0, 120)
            }.show()
        }
    }

    abstract fun showLoadingView()
    abstract fun hideLoadingView()
}