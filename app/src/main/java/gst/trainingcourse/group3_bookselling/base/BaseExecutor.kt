package gst.trainingcourse.group3_bookselling.base

import android.util.Log
import androidx.lifecycle.MutableLiveData
import gst.trainingcourse.group3_bookselling.util.Constants
import gst.trainingcourse.group3_bookselling.util.NetWorkState
import kotlinx.coroutines.*
import java.lang.Exception

abstract class BaseExecutor {
    protected val msgLiveData = MutableLiveData<String>()

    fun executeIO(scope: CoroutineScope, function: (suspend () -> Unit)) {
        if (NetWorkState.isConnected) {
            scope.launch(Dispatchers.IO) {
                try {
                    function.invoke()
                } catch (e: Exception) {
                    Log.d("AAA", "catch error ${e.message}")
                    withContext(Dispatchers.Main) {
                        handleExecuteError()
                    }
                }
            }
        } else {
            handleNetworkOff()
        }
    }

    private fun handleExecuteError() {
        msgLiveData.value = Constants.MSG_ERROR
    }

    private fun handleNetworkOff() {
        msgLiveData.value = Constants.MSG_NETWORK_OFF
    }
}