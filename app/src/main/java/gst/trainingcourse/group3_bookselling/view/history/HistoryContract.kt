package gst.trainingcourse.group3_bookselling.view.history

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.repository.entity.OrderHistory
import kotlinx.coroutines.CoroutineScope

interface HistoryContract {
    interface View{
        fun initAdapter()
        fun initView()
        fun initData()
        fun showDetailFragment(orderHistory: OrderHistory)
    }

    interface Presenter {
        fun observe(
            owner: LifecycleOwner,
            historyObserver: Observer<MutableList<OrderHistory>>,
            msgObserver: Observer<String>
        )

        fun loadHistory(scope: CoroutineScope)
    }
}