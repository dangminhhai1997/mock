package gst.trainingcourse.group3_bookselling.view.user

import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.repository.entity.UserInfo
import kotlinx.coroutines.CoroutineScope

interface UserContract {
    interface View {
        fun initAction()
        fun updateInfo(userInfo: UserInfo)
        fun showContent()
        fun hideContent()
        fun showSignInButton()
        fun showFragment(fragment: Fragment)
        fun showEditFragment()
    }

    interface Presenter {
        fun observe(
            owner: LifecycleOwner,
            userObserver: Observer<UserInfo?>,
            signInObserver: Observer<Boolean>,
            msgObserver: Observer<String>
        )

        fun getUserInfo(scope: CoroutineScope)
        fun handleSignOut(scope: CoroutineScope)
    }
}