package gst.trainingcourse.group3_bookselling.view.edit_info

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import gst.trainingcourse.group3_bookselling.base.BaseFragment
import gst.trainingcourse.group3_bookselling.databinding.FragmentEditInfoBinding
import gst.trainingcourse.group3_bookselling.repository.entity.UserInfo
import gst.trainingcourse.group3_bookselling.util.Constants
import gst.trainingcourse.group3_bookselling.view.dialog.FormattedDatePicker
import gst.trainingcourse.group3_bookselling.view.home.HomeActivity


class EditInfoFragment : BaseFragment(), EditInfoContract.View {
    private var binding: FragmentEditInfoBinding? = null
    private lateinit var userInfo: UserInfo
    private val presenter = EditInfoPresenter()

    private val updateObserver = Observer<Boolean> { isSuccess ->
        if (isSuccess) {
            (activity as? HomeActivity)?.popFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userInfo = arguments?.getParcelable(DATA_KEY) ?: UserInfo()
        presenter.observe(this, msgObserver, updateObserver)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentEditInfoBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initAction()
    }

    override fun initView() {
        binding?.apply {
            editInfoName.setText(userInfo.name)
            editInfoAddress.setText(userInfo.address)
            editInfoPhone.setText(userInfo.phone)
            editInfoBirth.text = userInfo.birth
            if (userInfo.gender == Constants.USER_GENDER_MALE) {
                editInfoMale.isChecked = true
            } else {
                editInfoFemale.isChecked = true
            }
        }
    }

    override fun initAction() {
        binding?.apply {
            editInfoMale.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) userInfo.gender = Constants.USER_GENDER_MALE
            }
            editInfoFemale.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) userInfo.gender = Constants.USER_GENDER_FEMALE
            }
            editInfoPickBirth.setOnClickListener { showDatePicker() }
            editInfoConfirm.setOnClickListener {
                if (clickable) {
                    userInfo.phone = editInfoPhone.text.toString().trim()
                    userInfo.address = editInfoAddress.text.toString().trim()
                    userInfo.name = editInfoName.text.toString().trim()
                    showLoadingView()
                    presenter.updateInfo(lifecycleScope, userInfo)
                }
            }
        }
    }

    override fun showDatePicker() {
        val picker = FormattedDatePicker { dateStr ->
            userInfo.birth = dateStr
            binding?.editInfoBirth?.text = dateStr
        }
        activity?.run {
            picker.show(supportFragmentManager, "")
        }
    }

    override fun showLoadingView() {
        clickable = false
        binding?.editInfoProgress?.visibility = View.VISIBLE
    }

    override fun hideLoadingView() {
        binding?.editInfoProgress?.visibility = View.INVISIBLE
        clickable = true
    }

    companion object {
        private const val DATA_KEY = "User"

        @JvmStatic
        fun newInstance(userInfo: UserInfo) = EditInfoFragment().apply {
            arguments = Bundle().apply {
                putParcelable(DATA_KEY, userInfo)
            }
        }
    }
}
