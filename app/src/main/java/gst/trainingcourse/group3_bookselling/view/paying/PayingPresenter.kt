package gst.trainingcourse.group3_bookselling.view.paying

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.base.BaseExecutor
import gst.trainingcourse.group3_bookselling.repository.entity.CartItem
import gst.trainingcourse.group3_bookselling.repository.source.CartRepository
import gst.trainingcourse.group3_bookselling.repository.source.UserRepository
import gst.trainingcourse.group3_bookselling.databinding.FragmentPayingBinding
import gst.trainingcourse.group3_bookselling.repository.entity.UserInfo
import gst.trainingcourse.group3_bookselling.util.Constants
import kotlinx.coroutines.*

class PayingPresenter : BaseExecutor(), PayingContract.Presenter {
    private var address = ""
    private var phone = ""
    private val interactLiveData = MutableLiveData<Int>()
    private val userLiveData = MutableLiveData<UserInfo>()
    private val userRepo = UserRepository()
    private val cartRepo = CartRepository()

    override fun observe(
        owner: LifecycleOwner,
        msgObserver: Observer<String>,
        interactObserver: Observer<Int>,
        userObserver: Observer<UserInfo>
    ) {
        msgLiveData.observe(owner, msgObserver)
        interactLiveData.observe(owner, interactObserver)
        userLiveData.observe(owner, userObserver)
    }

    override fun getDefaultPayInfo(scope: CoroutineScope) {
        executeIO(scope) {
            val userInfo = userRepo.getCurrentUserInfo()
            if (userInfo != null) {
                withContext(Dispatchers.Main) {
                    userLiveData.value = userInfo
                }
            }
        }
    }

    override fun confirm(binding: FragmentPayingBinding) {
        if (checkInputEmpty(binding)) {
            msgLiveData.value = Constants.MSG_EMPTY
        } else {
            interactLiveData.value = PayingFragment.COMMAND_CONFIRM
        }
    }

    override fun sendOrder(scope: CoroutineScope, cartItems: MutableList<CartItem>, total: Int) {
        executeIO(scope) {
            val isAddSuccess = cartRepo.addToHistory(cartItems, total)
            if (isAddSuccess) {
                //need send notification here
                cartRepo.clearCart()
                withContext(Dispatchers.Main) {
                    interactLiveData.value = PayingFragment.COMMAND_SUCCESS
                }
            } else {
                withContext(Dispatchers.Main) {
                    msgLiveData.value = Constants.MSG_ERROR
                }
            }
        }

    }

    private fun checkInputEmpty(binding: FragmentPayingBinding): Boolean {
        phone = binding.payDetail.payPhoneNumber.text.toString().trim()
        address = binding.payDetail.payAddress.text.toString().trim()
        return phone.isEmpty() || address.isEmpty()
    }
}