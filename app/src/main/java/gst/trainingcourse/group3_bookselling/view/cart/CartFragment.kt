package gst.trainingcourse.group3_bookselling.view.cart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import gst.trainingcourse.group3_bookselling.base.BaseFragment

import gst.trainingcourse.group3_bookselling.view.cart.adapter.CartItemAdapter
import gst.trainingcourse.group3_bookselling.repository.entity.CartItem
import gst.trainingcourse.group3_bookselling.databinding.FragmentCartBinding
import gst.trainingcourse.group3_bookselling.util.FullSpaceDivider

//private val TAG = CartFragment::class.java.simpleName

class CartFragment : BaseFragment(), CartContract.View {
    private var binding: FragmentCartBinding? = null
    private val presenter = CartPresenter()
    private lateinit var cartAdapter: CartItemAdapter

    private val cartInteractionListener = object : CartItemAdapter.OnInteractionListener {
        override fun onUpdateQuantity(cartItemId: String, quantity: Int) {
            if (clickable) {
                showLoadingView()
                presenter.updateQuantity(lifecycleScope, quantity, cartItemId)
            }
        }

        override fun onRemoveItem(cartItemId: String) {
            if (clickable) {
                showLoadingView()
                presenter.removeItem(lifecycleScope, cartItemId)
            }
        }
    }
    private val cartItemObserver = Observer<MutableList<CartItem>> { cartItems ->
        updateListCartView(cartItems)
        presenter.calculateTotal()
        hideLoadingView()
    }

    /**
     * hide oder view if total value == 0
     */
    private val orderObserver = Observer<String> {
        if ('0' == it[0]) {
            updateOrderingInfo(it)
            hideOrderingView()
        } else {
            updateOrderingInfo(it)
            showOrderingView()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = CartFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        cartAdapter = CartItemAdapter(ArrayList(), cartInteractionListener)
        presenter.observe(this, cartItemObserver, orderObserver, msgObserver)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCartBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun initView() {
        binding?.apply {
            cartRcv.setHasFixedSize(true)
            cartRcv.addItemDecoration(FullSpaceDivider(4))
            cartRcv.adapter = cartAdapter
            cartOrderBtn.setOnClickListener {
                if (clickable) {
                    presenter.confirmOrder(activity)
                    showLoadingView()
                }
            }
        }
        //try load cart items if cart adapter is empty
        if (cartAdapter.itemCount <= 0) {
            presenter.loadCart(lifecycleScope)
            showLoadingView()
        } else {//else calculate total and update result to view
            presenter.calculateTotal()
            showOrderingView()
        }
    }

    override fun updateOrderingInfo(total: String) {
        binding?.cartTotalText?.text = total
    }

    override fun hideOrderingView() {
        binding?.apply {
            payView.visibility = View.INVISIBLE
            cartNote.visibility = View.VISIBLE
        }
    }

    override fun showOrderingView() {
        binding?.payView?.visibility = View.VISIBLE
    }

    override fun updateListCartView(cartItemList: MutableList<CartItem>) {
        cartAdapter.setCartItemList(cartItemList)
    }

    override fun showLoadingView() {
        clickable = false
        binding?.cartLoadingBar?.show()
    }

    override fun hideLoadingView() {
        binding?.cartLoadingBar?.hide()
        clickable = true
    }
}
