package gst.trainingcourse.group3_bookselling.view.history.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import gst.trainingcourse.group3_bookselling.R
import gst.trainingcourse.group3_bookselling.base.BaseViewHolder
import gst.trainingcourse.group3_bookselling.databinding.ItemOrderHistoryBinding
import gst.trainingcourse.group3_bookselling.repository.entity.OrderHistory
import gst.trainingcourse.group3_bookselling.util.BookUtil

class HistoryAdapter(private val historyList: MutableList<OrderHistory>) :
    RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder>() {
    var onItemClick: ((OrderHistory) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemOrderHistoryBinding.inflate(inflater, parent, false)
        return HistoryViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return historyList.size
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        holder.bind(historyList[position])
    }

    fun setHistoryList(historyList: MutableList<OrderHistory>) {
        this.historyList.clear()
        this.historyList.addAll(historyList)
        notifyDataSetChanged()
    }

    inner class HistoryViewHolder(private val binding: ItemOrderHistoryBinding) :
        BaseViewHolder<OrderHistory>(binding.root) {

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(historyList[absoluteAdapterPosition])
            }
        }

        override fun bind(dataItem: OrderHistory) {
            binding.apply {
                val resource = root.resources
                orderHistoryItemId.text = resource.getString(R.string.order_id, dataItem.historyId)
                orderHistoryItemDate.text = resource.getString(R.string.order_date, dataItem.date)
                orderHistoryItemValue.text =
                    resource.getString(
                        R.string.total_value,
                        BookUtil.currencyFormat(dataItem.value)
                    )
                orderHistoryItemStatus.text =
                    resource.getString(R.string.delivery_status, dataItem.status)
            }
        }
    }
}