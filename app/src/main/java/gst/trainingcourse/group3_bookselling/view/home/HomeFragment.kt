package gst.trainingcourse.group3_bookselling.view.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import gst.trainingcourse.group3_bookselling.R
import gst.trainingcourse.group3_bookselling.base.BaseFragment
import gst.trainingcourse.group3_bookselling.view.home.adapter.BookAdapter
import gst.trainingcourse.group3_bookselling.view.home.adapter.CategoryAdapter
import gst.trainingcourse.group3_bookselling.repository.entity.Book
import gst.trainingcourse.group3_bookselling.databinding.FragmentHomeBinding

import gst.trainingcourse.group3_bookselling.util.Constants
import gst.trainingcourse.group3_bookselling.util.FullSpaceDivider

//private val TAG = HomeFragment::class.java.simpleName

class HomeFragment : BaseFragment(), HomeContract.View {
    private var presenter = HomePresenter()
    private var binding: FragmentHomeBinding? = null
    private lateinit var bookAdapter: BookAdapter
    private lateinit var categoryAdapter: CategoryAdapter

    private val bookObserver = Observer<MutableList<Book>> { books ->
        updateBookView(books)
        hideLoadingView()
    }

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupAdapters()
        presenter.observe(this, bookObserver, msgObserver)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        if (bookAdapter.itemCount <= 0) {
            categoryAdapter.clickAt(0)
            showLoadingView()
        }
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    override fun initView() {
        binding?.apply {
            homeCategoryRcv.adapter = categoryAdapter
            //homeCategoryRcv.setHasFixedSize(true)
            homeBookRcv.adapter = bookAdapter
            homeBookRcv.addItemDecoration(FullSpaceDivider(4))
        }
    }

    override fun setupAdapters() {
        bookAdapter = BookAdapter(ArrayList())
        bookAdapter.onItemClick = { book ->
            presenter.showBookDetailFragment(activity, book)
        }
        categoryAdapter = CategoryAdapter(context, Constants.categoryArray)
        categoryAdapter.onItemClick = { category ->

            presenter.loadBookByCategory(lifecycleScope, category.id)
            showLoadingView()
        }
    }

    override fun showLoadingView() {
        clickable = false
        binding?.homeLoadingBar?.show()
    }

    override fun hideLoadingView() {
        binding?.homeLoadingBar?.hide()
        clickable = true
    }

    override fun updateBookView(bookList: MutableList<Book>) {
        if (bookList.isEmpty()) {
            binding?.apply {
                homeNote.text = resources.getString(R.string.no_result_found)
                homeNote.visibility = View.VISIBLE
            }
        } else {
            binding?.homeNote?.visibility = View.INVISIBLE
        }
        bookAdapter.setBookList(bookList)
    }
}
