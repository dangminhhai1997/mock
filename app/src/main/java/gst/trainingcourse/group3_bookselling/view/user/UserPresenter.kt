package gst.trainingcourse.group3_bookselling.view.user

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.base.BaseExecutor
import gst.trainingcourse.group3_bookselling.repository.entity.UserInfo
import gst.trainingcourse.group3_bookselling.repository.source.UserRepository
import kotlinx.coroutines.*

class UserPresenter : BaseExecutor(), UserContract.Presenter {
    private val userLiveData = MutableLiveData<UserInfo>()
    private val signInLiveData = MutableLiveData<Boolean>()
    private val userRepo = UserRepository()

    override fun observe(
        owner: LifecycleOwner,
        userObserver: Observer<UserInfo?>,
        signInObserver: Observer<Boolean>,
        msgObserver: Observer<String>
    ) {
        userLiveData.observe(owner, userObserver)
        signInLiveData.observe(owner, signInObserver)
        msgLiveData.observe(owner, msgObserver)
    }

    override fun getUserInfo(scope: CoroutineScope) {
        executeIO(scope) {
            val currentUser = userRepo.getCurrentUserInfo()
            withContext(Dispatchers.Main) {
                userLiveData.value = currentUser
                msgLiveData.value = ""
            }
        }
    }

    override fun handleSignOut(scope: CoroutineScope) {
        if (userLiveData.value != null) {
            userRepo.logOut()
            userLiveData.value = null
        } else {
            signInLiveData.value = true
        }
        msgLiveData.value = ""
    }
}