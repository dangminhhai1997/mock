package gst.trainingcourse.group3_bookselling.view.login

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.R
import gst.trainingcourse.group3_bookselling.base.BaseExecutor
import gst.trainingcourse.group3_bookselling.repository.source.UserRepository
import gst.trainingcourse.group3_bookselling.util.Constants
import gst.trainingcourse.group3_bookselling.util.isValidEmail
import gst.trainingcourse.group3_bookselling.util.isValidPassword
import kotlinx.coroutines.*

class LoginPresenter : BaseExecutor(), LoginContract.Presenter {
    //login state
    private val loginLiveData = MutableLiveData<Boolean>()
    private val userRepo = UserRepository()

    override fun observe(
        owner: LifecycleOwner,
        msgObserver: Observer<String>,
        loginObserver: Observer<Boolean>
    ) {
        msgLiveData.observe(owner, msgObserver)
        loginLiveData.observe(owner, loginObserver)
    }

    /**
     * try sign up
     * check email and password is in correct form first
     * then check if this email is in database
     * if not, sign up and update result to view
     */
    override fun doSignUp(email: String, password: String, scope: CoroutineScope) {
        if (checkInput(email, password)) {
            executeIO(scope) {
                if (userRepo.checkUserExist(email)) {
                    withContext(Dispatchers.Main) {
                        msgLiveData.value = Constants.MSG_EMAIL_EXIST
                    }
                } else {
                    //try sign up
                    val isSignUpSuccess = userRepo.signUpWithEmail(email, password)
                    withContext(Dispatchers.Main) {
                        msgLiveData.value = Constants.MSG_SIGN_UP_SUCCESS
                        loginLiveData.value = isSignUpSuccess
                    }
                }
            }
        }
    }

    /**
     * try log in
     * check valid email or password first
     * then try log in and update result to view
     */
    override fun doLogIn(email: String, password: String, scope: CoroutineScope) {
        if (checkInput(email, password)) {
            executeIO(scope) {
                val isLoginSuccess = userRepo.logInWithEmail(email, password)
                withContext(Dispatchers.Main) {
                    if (isLoginSuccess) {
                        msgLiveData.value = Constants.MSG_LOG_IN_SUCCESS
                    } else {
                        msgLiveData.value = Constants.MSG_LOG_IN_FAIL
                    }
                    loginLiveData.value = isLoginSuccess
                }
            }
        }
    }

    /**
     * check email and password
     * update toast message if email or pass is invalid
     */
    override fun checkInput(email: String, password: String): Boolean {
        return when {
            !email.isValidEmail() -> {
                msgLiveData.value = Constants.MSG_EMAIL_INVALID
                false
            }
            !password.isValidPassword() -> {
                msgLiveData.value = Constants.MSG_PASSWORD_INVALID
                false
            }
            else -> true
        }
    }

    override fun updateKeepLogIn(isKeep: Boolean, context: Context) {
        val ref = context.getSharedPreferences(
            context.resources.getString(R.string.shared_ref_key),
            Context.MODE_PRIVATE
        )
        with(ref.edit()) {
            putBoolean(context.resources.getString(R.string.keep_logged_in_key), isKeep)
            apply()
        }
    }
}