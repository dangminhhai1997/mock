package gst.trainingcourse.group3_bookselling.view.paying

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.repository.entity.CartItem
import gst.trainingcourse.group3_bookselling.repository.entity.UserInfo
import gst.trainingcourse.group3_bookselling.databinding.FragmentPayingBinding
import kotlinx.coroutines.CoroutineScope

interface PayingContract {
    interface View  {
        fun initView()
        fun updatePayInfo(userInfo: UserInfo)
        fun showConfirmDialog()
        fun showSuccess()
    }

    interface Presenter{
        fun observe(
            owner: LifecycleOwner,
            msgObserver: Observer<String>,
            interactObserver: Observer<Int>,
            userObserver: Observer<UserInfo>
        )

        fun getDefaultPayInfo(scope: CoroutineScope)
        fun confirm(binding: FragmentPayingBinding)
        fun sendOrder(scope: CoroutineScope, cartItems: MutableList<CartItem>, total: Int)
    }
}