package gst.trainingcourse.group3_bookselling.view.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import gst.trainingcourse.group3_bookselling.base.BaseFragment

import gst.trainingcourse.group3_bookselling.databinding.FragmentHistoryBinding
import gst.trainingcourse.group3_bookselling.repository.entity.OrderHistory
import gst.trainingcourse.group3_bookselling.util.FullSpaceDivider
import gst.trainingcourse.group3_bookselling.view.history.adapter.HistoryAdapter
import gst.trainingcourse.group3_bookselling.view.history_detail.HistoryDetailFragment
import gst.trainingcourse.group3_bookselling.view.home.HomeActivity

class HistoryFragment : BaseFragment(), HistoryContract.View {
    private val presenter = HistoryPresenter()
    private var binding: FragmentHistoryBinding? = null
    private lateinit var historyAdapter: HistoryAdapter

    private val historyObserver = Observer<MutableList<OrderHistory>> { historyList ->
        historyAdapter.setHistoryList(historyList)
        hideLoadingView()
    }

    companion object {
        @JvmStatic
        fun newInstance() = HistoryFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initAdapter()
        presenter.observe(this, historyObserver, msgObserver)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHistoryBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initData()
    }

    override fun onDestroyView() {
        (activity as? HomeActivity)?.showBottom()
        binding = null
        super.onDestroyView()
    }

    override fun initAdapter() {
        historyAdapter = HistoryAdapter(ArrayList())
        historyAdapter.onItemClick = { historyItem ->
            if (clickable) {
                showDetailFragment(historyItem)
            }
        }
    }

    override fun initView() {
        binding?.apply {
            historyOrderRcv.adapter = historyAdapter
            historyOrderRcv.addItemDecoration(FullSpaceDivider(4))
        }
    }

    override fun initData() {
        if (historyAdapter.itemCount <= 0) {
            showLoadingView()
            presenter.loadHistory(lifecycleScope)
        }
    }

    override fun showDetailFragment(orderHistory: OrderHistory) {
        (activity as? HomeActivity)?.run {
            hideBottom()
            replaceFragment(HistoryDetailFragment.newInstance(orderHistory))
        }
    }

    override fun showLoadingView() {
        clickable = false
        binding?.historyProgress?.show()
    }

    override fun hideLoadingView() {
        binding?.historyProgress?.hide()
        clickable = true
    }
}
