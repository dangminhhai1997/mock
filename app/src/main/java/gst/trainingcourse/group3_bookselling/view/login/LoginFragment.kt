package gst.trainingcourse.group3_bookselling.view.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import gst.trainingcourse.group3_bookselling.base.BaseFragment

import gst.trainingcourse.group3_bookselling.databinding.FragmentLoginBinding
import gst.trainingcourse.group3_bookselling.view.home.HomeActivity

class LoginFragment : BaseFragment(),
    LoginContract.View {
    private var binding: FragmentLoginBinding? = null
    private var presenter = LoginPresenter()

    private val logInObserver = Observer<Boolean> { isLogInSuccess ->
        if (isLogInSuccess) {
            (activity as? HomeActivity)?.popFragment()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = LoginFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.observe(this, msgObserver, logInObserver)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAction()
    }

    override fun initAction() {
        binding?.apply {
            loginBtn.setOnClickListener {
                if (clickable) {
                    showLoadingView()
                    presenter.doLogIn(
                        loginEmail.text.toString().trim(),
                        loginPassword.text.toString().trim(),
                        lifecycleScope
                    )
                }
            }
            signUpBtn.setOnClickListener {
                if (clickable) {
                    showLoadingView()
                    presenter.doSignUp(
                        loginEmail.text.toString().trim(),
                        loginPassword.text.toString().trim(),
                        lifecycleScope
                    )
                }
            }
            loginKeepLogIn.setOnCheckedChangeListener { _, isChecked ->
                context?.run {
                    presenter.updateKeepLogIn(isChecked, this)
                }
            }
        }
    }

    override fun onDestroyView() {
        (activity as? HomeActivity)?.showBottom()
        binding = null
        super.onDestroyView()
    }

    override fun showLoadingView() {
        clickable = false
        binding?.loginProgress?.visibility = View.VISIBLE
    }

    override fun hideLoadingView() {
        binding?.loginProgress?.visibility = View.GONE
        clickable = true
    }
}
