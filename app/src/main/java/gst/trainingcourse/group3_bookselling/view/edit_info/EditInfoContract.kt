package gst.trainingcourse.group3_bookselling.view.edit_info

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.repository.entity.UserInfo
import kotlinx.coroutines.CoroutineScope

interface EditInfoContract {
    interface View {
        fun initView()
        fun initAction()
        fun showDatePicker()
    }

    interface Presenter {
        fun observe(
            owner: LifecycleOwner,
            msgObserver: Observer<String>,
            updateObserver: Observer<Boolean>
        )

        fun updateInfo(scope: CoroutineScope, info: UserInfo)
    }
}