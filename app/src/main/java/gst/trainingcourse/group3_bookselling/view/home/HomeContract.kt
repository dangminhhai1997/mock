package gst.trainingcourse.group3_bookselling.view.home

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.repository.entity.Book
import kotlinx.coroutines.CoroutineScope

interface HomeContract {
    interface View {
        fun setupAdapters()
        fun initView()
        fun updateBookView(bookList: MutableList<Book>)
    }

    interface Presenter {
        fun showBookDetailFragment(activity: FragmentActivity?, book: Book)
        fun observe(
            owner: LifecycleOwner,
            observer: Observer<MutableList<Book>>,
            msgObserver: Observer<String>
        )

        fun loadBookByCategory(scope: CoroutineScope, id: Int)
    }
}