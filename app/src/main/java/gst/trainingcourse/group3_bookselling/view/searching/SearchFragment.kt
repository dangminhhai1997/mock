package gst.trainingcourse.group3_bookselling.view.searching

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import gst.trainingcourse.group3_bookselling.R
import gst.trainingcourse.group3_bookselling.base.BaseFragment
import gst.trainingcourse.group3_bookselling.repository.entity.Book

import gst.trainingcourse.group3_bookselling.databinding.FragmentSearchBinding
import gst.trainingcourse.group3_bookselling.util.FullSpaceDivider
import gst.trainingcourse.group3_bookselling.view.home.adapter.BookAdapter

class SearchFragment : BaseFragment(), SearchContract.View {
    private var binding: FragmentSearchBinding? = null
    private var presenter = SearchPresenter()
    private lateinit var bookAdapter: BookAdapter

    private val bookObserver = Observer<MutableList<Book>> { books ->
        if (books.isEmpty()) {
            showNote()
        } else {
            bookAdapter.setBookList(books)
        }
        hideLoadingView()
    }

    companion object {
        @JvmStatic
        fun newInstance() = SearchFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.observe(this, bookObserver, msgObserver)
        bookAdapter = BookAdapter(ArrayList())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSearchBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAction()
    }

    override fun initAction() {
        bookAdapter.onItemClick = { book ->
            presenter.showDetailFragment(activity, book)
        }
        binding?.apply {
            searchBookRcv.adapter = bookAdapter
            searchBookRcv.addItemDecoration(FullSpaceDivider(4))
            searchBar.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    if (clickable && query != null) {
                        showLoadingView()
                        presenter.search(lifecycleScope, query)
                    }
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    //do nothing for now
                    return true
                }
            })
        }
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    override fun showNote() {
        binding?.searchNote?.text = resources.getString(R.string.no_result_found)
    }

    override fun showLoadingView() {
        clickable = false
        binding?.searchLoadingBar?.show()
    }

    override fun hideLoadingView() {
        binding?.searchLoadingBar?.hide()
        clickable = true
    }
}
