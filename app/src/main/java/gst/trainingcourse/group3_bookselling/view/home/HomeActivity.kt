package gst.trainingcourse.group3_bookselling.view.home

import android.content.Context
import android.net.*
import android.net.ConnectivityManager.NetworkCallback
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.FirebaseApp
import gst.trainingcourse.group3_bookselling.R
import gst.trainingcourse.group3_bookselling.databinding.ActivityHomeBinding
import gst.trainingcourse.group3_bookselling.repository.source.UserRepository
import gst.trainingcourse.group3_bookselling.util.NetWorkState
import gst.trainingcourse.group3_bookselling.view.cart.CartFragment
import gst.trainingcourse.group3_bookselling.view.searching.SearchFragment
import gst.trainingcourse.group3_bookselling.view.user.UserFragment

private val TAG = HomeActivity::class.java.simpleName

class HomeActivity : AppCompatActivity(R.layout.activity_home) {
    private val nameNotHome = "NotHome"
    private val networkCallback: NetworkCallback = object : NetworkCallback() {
        override fun onAvailable(network: Network) {
            NetWorkState.isConnected = true
        }

        override fun onLost(network: Network) {
            NetWorkState.isConnected = false
        }
    }
    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        FirebaseApp.initializeApp(applicationContext)
        initView()
        registerNetwork()
    }

    private fun initView() {
        setSupportActionBar(binding.homeToolBar)
        binding.bottomNavHome.setOnNavigationItemSelectedListener(mNavListener)
        addFragment(HomeFragment.newInstance(), TAG)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount <= 1) {
            finish()
        } else {
            super.onBackPressed()
            if (supportFragmentManager.backStackEntryCount == 1) {
                binding.bottomNavHome.selectedItemId = R.id.itemNavHome
            }
        }
    }

    /**
     * always keep home fragment at bottom of stack
     */
    private val mNavListener = BottomNavigationView.OnNavigationItemSelectedListener {
        val selectedItemId = it.itemId
        val manager = supportFragmentManager
        if (manager.backStackEntryCount > 1) {
            manager.popBackStack(nameNotHome, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
        val selectedFragment: Fragment = when (selectedItemId) {
            R.id.itemNavCart -> {
                CartFragment.newInstance()
            }
            R.id.itemNavSearch -> {
                SearchFragment.newInstance()
            }
            R.id.itemNavUser -> {
                UserFragment.newInstance()
            }

            else -> return@OnNavigationItemSelectedListener true
        }
        replaceFragment(selectedFragment)
        true
    }

    private fun addFragment(fragment: Fragment, tag: String) {
        supportFragmentManager.beginTransaction()
            .add(R.id.homeContainer, fragment, tag)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .addToBackStack(null)
            .commit()
    }

    fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.homeContainer, fragment)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .addToBackStack(nameNotHome)
            .commit()
    }

    fun popFragment() {
        supportFragmentManager.popBackStack()
    }

    fun hideBottom() {
        binding.bottomNavHome.visibility = View.GONE
    }

    fun showBottom() {
        binding.bottomNavHome.visibility = View.VISIBLE
    }

    fun setToolBarTitle(title: String) {
        supportActionBar?.title = title
    }

    private fun registerNetwork() {
        val networkRequest = NetworkRequest.Builder()
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .build()
        val manager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        manager.registerNetworkCallback(networkRequest, networkCallback)
    }

    private fun unregisterNetwork() {
        val manager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        manager.unregisterNetworkCallback(networkCallback)
    }

    override fun onDestroy() {
        unregisterNetwork()
        val ref =
            getSharedPreferences(resources.getString(R.string.shared_ref_key), Context.MODE_PRIVATE)
        val stayLoggedIn = ref.getBoolean(resources.getString(R.string.keep_logged_in_key), false)
        if (!stayLoggedIn) UserRepository().logOut()
        super.onDestroy()
    }
}
