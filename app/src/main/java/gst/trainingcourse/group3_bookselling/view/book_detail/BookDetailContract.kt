package gst.trainingcourse.group3_bookselling.view.book_detail

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.repository.entity.Book
import kotlinx.coroutines.CoroutineScope

interface BookDetailContract {
    interface View {
        fun init()
        fun showLogIn()
    }

    interface Presenter {
        fun addToCart(scope: CoroutineScope, book: Book)
        fun observe(
            owner: LifecycleOwner,
            logInObserver: Observer<Boolean>,
            msgObserver: Observer<String>
        )
    }
}