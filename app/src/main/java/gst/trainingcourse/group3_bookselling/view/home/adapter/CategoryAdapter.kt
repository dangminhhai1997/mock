package gst.trainingcourse.group3_bookselling.view.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import gst.trainingcourse.group3_bookselling.R
import gst.trainingcourse.group3_bookselling.base.BaseViewHolder
import gst.trainingcourse.group3_bookselling.repository.entity.Category
import gst.trainingcourse.group3_bookselling.databinding.ItemCategoryBinding

//private val TAG = CategoryAdapter::class.java.simpleName

class CategoryAdapter(
    context: Context?,
    private val categoryList: Array<Category>
) : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {
    private var selectedItem = -1
    private var colorHighlight = 0
    private var colorNormal = 0
    var onItemClick: ((Category) -> Unit)? = null

    init {
        //get color resource
        context?.run {
            colorHighlight = ResourcesCompat.getColor(resources, R.color.colorDarkLightGreen, null)
            colorNormal = ResourcesCompat.getColor(resources, R.color.colorGray, null)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCategoryBinding.inflate(inflater, parent, false)
        return CategoryViewHolder(binding)
    }

    /**
     * select an item and trigger its onItemClick
     * @param position -> item's position
     */
    fun clickAt(position: Int) {
        if (categoryList.isEmpty() && (position !in categoryList.indices)) return
        highlightItem(position)
        onItemClick?.invoke(categoryList[position])
    }

    /**
     * highlight selected item and normalize previous selected item
     * @param position -> position to highlight
     */
    private fun highlightItem(position: Int) {
        if (position == selectedItem) return
        val oldPos = selectedItem
        selectedItem = position
        notifyItemChanged(oldPos)
        notifyItemChanged(selectedItem)
    }

    override fun getItemCount(): Int {
        return categoryList.size
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.bind(categoryList[position])
    }

    override fun onBindViewHolder(
        holder: CategoryViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (position == selectedItem) {
            holder.select(colorHighlight)
        } else {
            holder.deselect(colorNormal)
        }
        super.onBindViewHolder(holder, position, payloads)
    }

    inner class CategoryViewHolder(private val binding: ItemCategoryBinding) :
        BaseViewHolder<Category>(binding.root) {

        override fun bind(dataItem: Category) {
            binding.itemCategoryName.text = dataItem.name
            itemView.setOnClickListener {
                highlightItem(absoluteAdapterPosition)
                onItemClick?.invoke(categoryList[absoluteAdapterPosition])
            }
        }

        fun select(color: Int) {
            binding.root.setBackgroundResource(R.drawable.bg_category_selected)
            binding.itemCategoryName.setTextColor(color)
        }

        fun deselect(color: Int) {
            binding.root.setBackgroundResource(R.drawable.bg_empty)
            binding.itemCategoryName.setTextColor(color)
        }
    }
}

