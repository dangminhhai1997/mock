package gst.trainingcourse.group3_bookselling.view.book_detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import gst.trainingcourse.group3_bookselling.base.BaseFragment

import gst.trainingcourse.group3_bookselling.repository.entity.Book
import gst.trainingcourse.group3_bookselling.databinding.FragmentBookDetailBinding
import gst.trainingcourse.group3_bookselling.util.BookUtil
import gst.trainingcourse.group3_bookselling.view.home.HomeActivity
import gst.trainingcourse.group3_bookselling.view.home.adapter.BookAdapter
import gst.trainingcourse.group3_bookselling.view.login.LoginFragment

class BookDetailFragment : BaseFragment(), BookDetailContract.View {
    private val presenter = BookDetailPresenter()
    private var binding: FragmentBookDetailBinding? = null
    private lateinit var book: Book

    companion object {
        const val BOOK_KEY = "book"

        @JvmStatic
        fun newInstance(book: Book) =
            BookDetailFragment().apply {
                arguments = Bundle().apply { putParcelable(BOOK_KEY, book) }
            }
    }

    private val logInObserver = Observer<Boolean> { isNeedLogIn ->
        if (isNeedLogIn) {
            showLogIn()
        }
        hideLoadingView()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            book = it.getParcelable(BOOK_KEY) ?: Book()
        }
        presenter.observe(this, logInObserver, msgObserver)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBookDetailBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    override fun init() {
        binding?.detailHeader?.apply {
            Glide.with(detailBigImage)
                .load(book.imageUrl)
                .fitCenter()
                .apply(BookAdapter.bookImageOption)
                .into(detailBigImage)
            detailTitle.text = book.bookName
            detailPrice.text = BookUtil.currencyFormat(book.price)
            detailAddBtn.setOnClickListener {
                showLoadingView()
                presenter.addToCart(lifecycleScope, book)
            }
        }
        binding?.detailContent?.apply {
            detailAuthor.text = book.author
            detailCategory.text = book.category
            detailPublisher.text = book.publisher
            detailPublishYear.text = book.publishYear.toString()
        }
    }

    override fun showLoadingView() {
        clickable = false
        binding?.detailLoading?.show()
    }

    override fun hideLoadingView() {
        binding?.detailLoading?.hide()
        clickable = true
    }

    override fun showLogIn() {
        (activity as? HomeActivity)?.replaceFragment(LoginFragment.newInstance())
    }
}
