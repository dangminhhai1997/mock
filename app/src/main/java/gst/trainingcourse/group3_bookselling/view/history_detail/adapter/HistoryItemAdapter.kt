package gst.trainingcourse.group3_bookselling.view.history_detail.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import gst.trainingcourse.group3_bookselling.base.BaseViewHolder
import gst.trainingcourse.group3_bookselling.databinding.ItemCartSimpleBinding
import gst.trainingcourse.group3_bookselling.repository.entity.HistoryItem
import gst.trainingcourse.group3_bookselling.util.BookUtil

class HistoryItemAdapter(private var itemList: MutableList<HistoryItem>) :
    RecyclerView.Adapter<HistoryItemAdapter.HistoryItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCartSimpleBinding.inflate(inflater, parent, false)
        return HistoryItemViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: HistoryItemViewHolder, position: Int) {
        holder.bind(itemList[position])
    }

    fun setItems(itemList: MutableList<HistoryItem>) {
        this.itemList.clear()
        this.itemList.addAll(itemList)
        notifyDataSetChanged()
    }

    inner class HistoryItemViewHolder(private val binding: ItemCartSimpleBinding) :
        BaseViewHolder<HistoryItem>(binding.root) {
        init {
            binding.itemCartSimpleImage.visibility = View.GONE
        }

        override fun bind(dataItem: HistoryItem) {
            binding.apply {
                itemCartSimpleTitle.text = dataItem.bookName
                itemCartSimplePrice.text = BookUtil.currencyFormat(dataItem.price)
                itemCartSQuantity.text = dataItem.quantity.toString()
            }
        }
    }
}