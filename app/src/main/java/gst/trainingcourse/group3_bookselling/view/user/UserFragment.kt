package gst.trainingcourse.group3_bookselling.view.user

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import gst.trainingcourse.group3_bookselling.R
import gst.trainingcourse.group3_bookselling.base.BaseFragment
import gst.trainingcourse.group3_bookselling.repository.entity.UserInfo

import gst.trainingcourse.group3_bookselling.databinding.FragmentUserBinding
import gst.trainingcourse.group3_bookselling.util.Constants
import gst.trainingcourse.group3_bookselling.view.edit_info.EditInfoFragment
import gst.trainingcourse.group3_bookselling.view.history.HistoryFragment
import gst.trainingcourse.group3_bookselling.view.home.HomeActivity
import gst.trainingcourse.group3_bookselling.view.login.LoginFragment

class UserFragment : BaseFragment(), UserContract.View {
    private var binding: FragmentUserBinding? = null
    private val presenter = UserPresenter()
    private lateinit var userInfo: UserInfo

    private val userObserver = Observer<UserInfo?> { user ->
        //if user info != null, show it
        if (user != null) {
            userInfo = user
            updateInfo(user)
            showContent()
        } else {// else hide detail view and show sign up button
            userInfo = UserInfo()
            hideContent()
            showSignInButton()
        }
        hideLoadingView()
    }

    private val logInObserver = Observer<Boolean> { isNeedLogIn ->
        if (isNeedLogIn) {
            showFragment(LoginFragment.newInstance())
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = UserFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.observe(this, userObserver, logInObserver, msgObserver)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentUserBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAction()
        presenter.getUserInfo(lifecycleScope)
        showLoadingView()
    }

    override fun initAction() {
        hideContent()
        //set onclick to sign out button
        binding?.apply {
            userSignOut.setOnClickListener {
                if (clickable) {
                    presenter.handleSignOut(lifecycleScope)
                    showLoadingView()
                }
            }
            userDetailLayout.setOnClickListener { showEditFragment() }
            userHeaderLayout.setOnClickListener { showEditFragment() }
            userBirth.setOnClickListener { showEditFragment() }
            userGender.setOnClickListener { showEditFragment() }
        }
    }

    override fun updateInfo(userInfo: UserInfo) {
        binding?.apply {
            userName.text = if (userInfo.name.isEmpty()) {
                Constants.USER_NAME_UNDEFINED
            } else {
                userInfo.name
            }
            userEmail.text = userInfo.email
            userBirth.text = if (userInfo.birth.isEmpty()) {
                Constants.USER_INFO_UNDEFINED
            } else {
                userInfo.birth
            }
            userGender.text = if (userInfo.gender.isEmpty()) {
                Constants.USER_INFO_UNDEFINED
            } else {
                userInfo.gender
            }
            userPhone.text = if (userInfo.phone.isEmpty()) {
                Constants.USER_INFO_UNDEFINED
            } else {
                userInfo.phone
            }
            userAddress.text = if (userInfo.address.isEmpty()) {
                Constants.USER_INFO_UNDEFINED
            } else {
                userInfo.phone
            }
            userHistory.setOnClickListener {
                showFragment(HistoryFragment.newInstance())
            }
        }
    }

    override fun showSignInButton() {
        binding?.apply {
            userSignOut.visibility = View.VISIBLE
            userSignOut.text = resources.getString(R.string.log_in)
            userHelloImage.visibility = View.VISIBLE
        }
    }

    override fun showEditFragment() {
        showFragment(EditInfoFragment.newInstance(userInfo))
    }

    override fun showFragment(fragment: Fragment) {
        (activity as? HomeActivity)?.run {
            hideBottom()
            replaceFragment(fragment)
        }
    }

    override fun showContent() {
        binding?.apply {
            userHeaderLayout.visibility = View.VISIBLE
            userDetailLayout.visibility = View.VISIBLE
            userOtherInfoLayout.visibility = View.VISIBLE
            userSignOut.visibility = View.VISIBLE
            userSignOut.text = resources.getString(R.string.log_out)
        }
    }

    override fun hideContent() {
        binding?.apply {
            userHeaderLayout.visibility = View.INVISIBLE
            userDetailLayout.visibility = View.INVISIBLE
            userOtherInfoLayout.visibility = View.INVISIBLE
            userSignOut.visibility = View.INVISIBLE
            userHelloImage.visibility = View.GONE
        }
    }

    override fun showLoadingView() {
        clickable = false
        binding?.userProgress?.show()
    }

    override fun hideLoadingView() {
        binding?.userProgress?.hide()
        clickable = true
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

}
