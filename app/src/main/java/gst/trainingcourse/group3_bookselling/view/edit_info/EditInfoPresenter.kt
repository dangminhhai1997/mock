package gst.trainingcourse.group3_bookselling.view.edit_info

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.base.BaseExecutor
import gst.trainingcourse.group3_bookselling.repository.entity.UserInfo
import gst.trainingcourse.group3_bookselling.repository.source.UserRepository
import gst.trainingcourse.group3_bookselling.util.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class EditInfoPresenter : BaseExecutor(), EditInfoContract.Presenter {
    private val updateLiveData = MutableLiveData<Boolean>()
    private val userRepository = UserRepository()

    override fun observe(
        owner: LifecycleOwner,
        msgObserver: Observer<String>,
        updateObserver: Observer<Boolean>
    ) {
        msgLiveData.observe(owner, msgObserver)
        updateLiveData.observe(owner, updateObserver)
    }

    override fun updateInfo(scope: CoroutineScope, info: UserInfo) {
        executeIO(scope) {
            val result = userRepository.updateUserInfo(info)
            withContext(Dispatchers.Main) {
                if (result) {
                    msgLiveData.value = Constants.MSG_UPDATE_INFO_SUCCESS
                    updateLiveData.value = true
                } else {
                    msgLiveData.value = Constants.MSG_UPDATE_INFO_FAILED
                }
            }
        }
    }

}