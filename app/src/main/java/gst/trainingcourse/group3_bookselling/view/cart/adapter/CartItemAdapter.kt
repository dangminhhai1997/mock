package gst.trainingcourse.group3_bookselling.view.cart.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import gst.trainingcourse.group3_bookselling.base.BaseViewHolder
import gst.trainingcourse.group3_bookselling.repository.entity.CartItem
import gst.trainingcourse.group3_bookselling.databinding.ItemCartBinding
import gst.trainingcourse.group3_bookselling.databinding.ItemCartSimpleBinding
import gst.trainingcourse.group3_bookselling.util.BookUtil
import gst.trainingcourse.group3_bookselling.util.CartItemDiffUtil
import gst.trainingcourse.group3_bookselling.view.home.adapter.BookAdapter
import java.lang.ClassCastException
import java.lang.Exception

val TAG = CartItemAdapter::class.java.simpleName
private const val TYPE_NORMAL = 1
private const val TYPE_SIMPLE = 2

class CartItemAdapter(
    private var itemList: MutableList<CartItem>,
    private val interactListener: OnInteractionListener?
) : RecyclerView.Adapter<BaseViewHolder<CartItem>>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<CartItem> {
        val inflater = LayoutInflater.from(parent.context)
        return if (viewType == TYPE_NORMAL) {
            val binding = ItemCartBinding.inflate(inflater, parent, false)
            CartViewHolder(binding, interactListener)
        } else {
            val binding = ItemCartSimpleBinding.inflate(inflater, parent, false)
            CartSimpleViewHolder(binding)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (interactListener == null) TYPE_SIMPLE else TYPE_NORMAL
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder<CartItem>, position: Int) {
        holder.bind(itemList[position])
    }

    override fun onBindViewHolder(
        holder: BaseViewHolder<CartItem>,
        position: Int,
        payloads: MutableList<Any>
    ) {
        //if holder is  an item in cart, which can interact with
        if (holder is CartViewHolder) {
            if (payloads.isEmpty()) {
                super.onBindViewHolder(holder, position, payloads)
            } else {//if payload is not empty
                try {//try update quantity
                    holder.bindQuantity(payloads[0] as Int)
                } catch (e: ClassCastException) {
                    Log.e(TAG, "#onBindViewHolder(..payloads) error + ${e.message}")
                }
            }
        } else {
            super.onBindViewHolder(holder, position, payloads)
        }
    }

    fun setCartItemList(newList: MutableList<CartItem>) {
        Log.d(TAG, "#setCartItemList() ${itemList.size}, ${newList.size}")
        try {
            val diffResult = DiffUtil.calculateDiff(CartItemDiffUtil(this.itemList, newList))
            this.itemList.clear()
            this.itemList.addAll(newList)
            diffResult.dispatchUpdatesTo(this)
        } catch (e: Exception) {
            Log.e(TAG, "#setCartItemList() error = ${e.message}")
            this.itemList.clear()
            this.itemList.addAll(newList)
        }
    }

    inner class CartSimpleViewHolder(private val binding: ItemCartSimpleBinding) :
        BaseViewHolder<CartItem>(binding.root) {
        override fun bind(dataItem: CartItem) {
            binding.apply {
                Glide.with(itemCartSimpleImage)
                    .load(dataItem.book.imageUrl)
                    .fitCenter()
                    .apply(BookAdapter.bookImageOption)
                    .into(itemCartSimpleImage)
                itemCartSimpleTitle.text = dataItem.book.bookName
                itemCartSimplePrice.text = BookUtil.currencyFormat(dataItem.book.price)
                itemCartSQuantity.text = dataItem.quantity.toString()
            }
        }
    }

    inner class CartViewHolder(
        private val binding: ItemCartBinding,
        private val listener: OnInteractionListener?
    ) : BaseViewHolder<CartItem>(binding.root) {
        init {
            binding.apply {
                itemCartDecBtn.setOnClickListener {
                    val item = itemList[absoluteAdapterPosition]
                    listener?.onUpdateQuantity(item.id, item.quantity - 1)
                }
                itemCartIncBtn.setOnClickListener {
                    val item = itemList[absoluteAdapterPosition]
                    listener?.onUpdateQuantity(item.id, item.quantity + 1)
                }
                itemCartRemoveBtn.setOnClickListener {
                    listener?.onRemoveItem(itemList[absoluteAdapterPosition].id)
                }
            }
        }

        override fun bind(dataItem: CartItem) {
            binding.apply {
                Glide.with(itemCartBookImage)
                    .load(dataItem.book.imageUrl)
                    //.diskCacheStrategy(DiskCacheStrategy.NONE)
                    //.skipMemoryCache(true)
                    .fitCenter()
                    .apply(BookAdapter.bookImageOption)
                    .into(itemCartBookImage)
                itemCartBookName.text = dataItem.book.bookName
                itemCartBookPrice.text = BookUtil.currencyFormat(dataItem.book.price)
                itemCartQuantityText.text = dataItem.quantity.toString()
            }
        }

        fun bindQuantity(quantity: Int) {
            binding.itemCartQuantityText.text = quantity.toString()
        }
    }

    interface OnInteractionListener {
        fun onUpdateQuantity(cartItemId: String, quantity: Int)
        fun onRemoveItem(cartItemId: String)
    }
}