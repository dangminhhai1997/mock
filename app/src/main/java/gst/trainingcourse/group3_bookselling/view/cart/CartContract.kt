package gst.trainingcourse.group3_bookselling.view.cart

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.repository.entity.CartItem
import kotlinx.coroutines.CoroutineScope

interface CartContract {
    interface View {
        fun updateOrderingInfo(total: String)
        fun showOrderingView()
        fun hideOrderingView()
        fun initView()
        fun updateListCartView(cartItemList: MutableList<CartItem>)
    }

    interface Presenter {
        fun observe(
            owner: LifecycleOwner,
            cartObserver: Observer<MutableList<CartItem>>,
            orderObserver: Observer<String>,
            msgObserver: Observer<String>
        )

        fun calculateTotal()
        fun removeItem(scope: CoroutineScope, cartItemId: String)
        fun updateQuantity(scope: CoroutineScope, quantity: Int, cartItemId: String)
        fun loadCart(scope: CoroutineScope)
        fun confirmOrder(activity: FragmentActivity?)
    }
}