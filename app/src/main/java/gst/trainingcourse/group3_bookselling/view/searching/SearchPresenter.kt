package gst.trainingcourse.group3_bookselling.view.searching

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.base.BaseExecutor
import gst.trainingcourse.group3_bookselling.repository.entity.Book
import gst.trainingcourse.group3_bookselling.repository.source.BookRepository
import gst.trainingcourse.group3_bookselling.view.book_detail.BookDetailFragment
import gst.trainingcourse.group3_bookselling.view.home.HomeActivity
import kotlinx.coroutines.*

class SearchPresenter : BaseExecutor(), SearchContract.Presenter {
    private var bookLiveData = MutableLiveData<MutableList<Book>>()
    private var searchType = TYPE_NAME
    private val bookRepo = BookRepository()

    companion object {
        const val TYPE_NAME = 0
        const val TYPE_AUTHOR = 1
    }

    override fun observe(
        owner: LifecycleOwner,
        bookObserver: Observer<MutableList<Book>>,
        msgObserver: Observer<String>
    ) {
        bookLiveData.observe(owner, bookObserver)
        msgLiveData.observe(owner, msgObserver)
    }

    override fun changeType(type: Int) {
        if ((type != TYPE_NAME) && (type != TYPE_AUTHOR)) {
            searchType = type
        }
    }

    override fun search(scope: CoroutineScope, keyword: String) {
        executeIO(scope) {
            val result = if (searchType == TYPE_NAME) {
                bookRepo.searchByName(keyword)
            } else {
                bookRepo.searchByAuthor(keyword)
            }
            withContext(Dispatchers.Main) {
                bookLiveData.value = result
                msgLiveData.value = ""
            }
        }
    }

    override fun showDetailFragment(activity: FragmentActivity?, book: Book) {
        (activity as? HomeActivity)?.replaceFragment(BookDetailFragment.newInstance(book))
    }
}