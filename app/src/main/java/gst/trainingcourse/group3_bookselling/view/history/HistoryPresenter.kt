package gst.trainingcourse.group3_bookselling.view.history

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.base.BaseExecutor
import gst.trainingcourse.group3_bookselling.repository.entity.OrderHistory
import gst.trainingcourse.group3_bookselling.repository.source.HistoryRepository
import gst.trainingcourse.group3_bookselling.util.Constants
import kotlinx.coroutines.*

class HistoryPresenter : BaseExecutor(), HistoryContract.Presenter {
    private val historyLiveData = MutableLiveData<MutableList<OrderHistory>>()
    private val historyRepo = HistoryRepository()

    override fun observe(
        owner: LifecycleOwner,
        historyObserver: Observer<MutableList<OrderHistory>>,
        msgObserver: Observer<String>
    ) {
        historyLiveData.observe(owner, historyObserver)
        msgLiveData.observe(owner, msgObserver)
    }

    override fun loadHistory(scope: CoroutineScope) {
        executeIO(scope){
            val historyList = historyRepo.getHistory()
            withContext(Dispatchers.Main) {
                historyLiveData.value = historyList
            }
        }
    }
}