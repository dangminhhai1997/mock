package gst.trainingcourse.group3_bookselling.view.history_detail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager

import gst.trainingcourse.group3_bookselling.R
import gst.trainingcourse.group3_bookselling.base.BaseFragment
import gst.trainingcourse.group3_bookselling.databinding.FragmentHistoryDetailBinding
import gst.trainingcourse.group3_bookselling.repository.entity.HistoryItem
import gst.trainingcourse.group3_bookselling.repository.entity.OrderHistory
import gst.trainingcourse.group3_bookselling.util.BookUtil
import gst.trainingcourse.group3_bookselling.util.FullSpaceDivider
import gst.trainingcourse.group3_bookselling.view.history_detail.adapter.HistoryItemAdapter


class HistoryDetailFragment : BaseFragment(), HistoryDetailContract.View {
    private lateinit var orderHistory: OrderHistory
    private lateinit var historyItemAdapter: HistoryItemAdapter
    private var binding: FragmentHistoryDetailBinding? = null
    private val presenter = HistoryDetailPresenter()

    private val historyItemObserver = Observer<MutableList<HistoryItem>> { items ->
        historyItemAdapter.setItems(items)
        hideLoadingView()
    }

    companion object {
        private const val DATA_KEY = "History"

        @JvmStatic
        fun newInstance(orderHistory: OrderHistory) =
            HistoryDetailFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(DATA_KEY, orderHistory)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            orderHistory = it.getParcelable(DATA_KEY) ?: OrderHistory()
        }
        presenter.observe(this, historyItemObserver, msgObserver)
        historyItemAdapter = HistoryItemAdapter(ArrayList())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHistoryDetailBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initData()
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    override fun initView() {
        binding?.apply {
            hisDetailItems.adapter = historyItemAdapter
            hisDetailItems.addItemDecoration(FullSpaceDivider(4))
            hisDetailHeader.apply {
                orderHistoryItemId.text =
                    resources.getString(R.string.order_id, orderHistory.historyId)
                orderHistoryItemDate.text =
                    resources.getString(R.string.order_date, orderHistory.date)
                orderHistoryItemValue.text =
                    resources.getString(
                        R.string.total_value,
                        BookUtil.currencyFormat(orderHistory.value)
                    )
                orderHistoryItemStatus.text =
                    resources.getString(R.string.delivery_status, orderHistory.status)
            }
        }
    }

    override fun initData() {
        presenter.loadHistoryItems(lifecycleScope, orderHistory.historyId)
        showLoadingView()
    }

    override fun showLoadingView() {
        clickable = false
        binding?.historyDetailProgress?.show()
    }

    override fun hideLoadingView() {
        binding?.historyDetailProgress?.hide()
        clickable = true
    }
}
