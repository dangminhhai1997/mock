package gst.trainingcourse.group3_bookselling.view.book_detail

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.base.BaseExecutor
import gst.trainingcourse.group3_bookselling.repository.entity.Book
import gst.trainingcourse.group3_bookselling.repository.source.CartRepository
import gst.trainingcourse.group3_bookselling.repository.source.UserRepository
import gst.trainingcourse.group3_bookselling.util.Constants
import kotlinx.coroutines.*

class BookDetailPresenter : BaseExecutor(), BookDetailContract.Presenter {
    private val logInLiveData = MutableLiveData<Boolean>()
    private val userRepo = UserRepository()
    private val cartRepo = CartRepository()

    override fun addToCart(scope: CoroutineScope, book: Book) {
        executeIO(scope) {
            if (userRepo.isLoggedIn()) {
                val addResult = cartRepo.addToCart(book)
                withContext(Dispatchers.Main) {
                    if (addResult) {
                        msgLiveData.value = Constants.MSG_ADD_CART_SUCCESS
                    } else {
                        msgLiveData.value = Constants.MSG_ADD_CART_FAILED
                    }
                }
            } else {
                withContext(Dispatchers.Main) {
                    logInLiveData.value = true
                }
            }
        }
    }

    override fun observe(
        owner: LifecycleOwner,
        logInObserver: Observer<Boolean>,
        msgObserver: Observer<String>
    ) {
        logInLiveData.observe(owner, logInObserver)
        msgLiveData.observe(owner, msgObserver)
    }
}