package gst.trainingcourse.group3_bookselling.view.home

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.base.BaseExecutor
import gst.trainingcourse.group3_bookselling.repository.entity.Book
import gst.trainingcourse.group3_bookselling.repository.source.BookRepository
import gst.trainingcourse.group3_bookselling.view.book_detail.BookDetailFragment
import kotlinx.coroutines.*

//private val TAG = HomePresenter::class.java.simpleName

class HomePresenter : BaseExecutor(), HomeContract.Presenter {
    private val bookLiveData = MutableLiveData<MutableList<Book>>()
    private val bookRepo = BookRepository()
    private var selectedCategory = -1
    private var currentJob: Job? = null

    override fun showBookDetailFragment(activity: FragmentActivity?, book: Book) {
        (activity as? HomeActivity)?.replaceFragment(BookDetailFragment.newInstance(book))
    }

    override fun observe(
        owner: LifecycleOwner,
        observer: Observer<MutableList<Book>>,
        msgObserver: Observer<String>
    ) {
        bookLiveData.observe(owner, observer)
        msgLiveData.observe(owner, msgObserver)
    }

    /**
     * load book list by category
     * if category is current -> do nothing
     * if the previous load is not completed -> cancel then load a new one
     *
     * @param scope -> coroutine scope to launch load book function
     * @param id -> category id
     */
    override fun loadBookByCategory(scope: CoroutineScope, id: Int) {
        if (id != selectedCategory) {
            // need cancel current?
            currentJob?.cancel()
            selectedCategory = id
            executeIO(scope) {
                val result = bookRepo.getByCategory(id)
                withContext(Dispatchers.Main) {
                    bookLiveData.value = result
                }
            }
        }
    }
}