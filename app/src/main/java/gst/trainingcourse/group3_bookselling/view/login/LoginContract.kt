package gst.trainingcourse.group3_bookselling.view.login

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import kotlinx.coroutines.CoroutineScope

interface LoginContract {
    interface View {
        fun initAction()
    }

    interface Presenter {
        fun observe(
            owner: LifecycleOwner,
            msgObserver: Observer<String>,
            loginObserver: Observer<Boolean>
        )

        fun updateKeepLogIn(isKeep: Boolean, context: Context)
        fun doSignUp(email: String, password: String, scope: CoroutineScope)
        fun checkInput(email: String, password: String): Boolean
        fun doLogIn(email: String, password: String, scope: CoroutineScope)
    }
}