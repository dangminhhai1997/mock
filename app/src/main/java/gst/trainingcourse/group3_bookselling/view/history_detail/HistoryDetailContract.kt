package gst.trainingcourse.group3_bookselling.view.history_detail

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.repository.entity.HistoryItem
import kotlinx.coroutines.CoroutineScope

interface HistoryDetailContract {
    interface View {
        fun initView()
        fun initData()
    }

    interface Presenter  {
        fun observe(
            owner: LifecycleOwner,
            historyItemObserver: Observer<MutableList<HistoryItem>>,
            msgObserver: Observer<String>
        )

        fun loadHistoryItems(scope: CoroutineScope, orderId: String)
    }
}