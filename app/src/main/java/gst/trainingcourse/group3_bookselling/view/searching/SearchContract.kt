package gst.trainingcourse.group3_bookselling.view.searching

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.repository.entity.Book
import kotlinx.coroutines.CoroutineScope

interface SearchContract {
    interface View{
        fun initAction()
        fun showNote()
    }

    interface Presenter {
        fun observe(
            owner: LifecycleOwner,
            bookObserver: Observer<MutableList<Book>>,
            msgObserver: Observer<String>
        )

        fun changeType(type: Int)
        fun search(scope: CoroutineScope, keyword: String)
        fun showDetailFragment(activity: FragmentActivity?, book: Book)
    }
}