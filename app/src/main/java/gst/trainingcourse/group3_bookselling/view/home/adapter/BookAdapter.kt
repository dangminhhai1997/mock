package gst.trainingcourse.group3_bookselling.view.home.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.request.RequestOptions
import gst.trainingcourse.group3_bookselling.R
import gst.trainingcourse.group3_bookselling.base.BaseViewHolder
import gst.trainingcourse.group3_bookselling.repository.entity.Book
import gst.trainingcourse.group3_bookselling.databinding.ItemBookBinding
import gst.trainingcourse.group3_bookselling.util.BookDiffUtil
import gst.trainingcourse.group3_bookselling.util.BookUtil
import gst.trainingcourse.group3_bookselling.view.cart.adapter.TAG
import java.lang.Exception

class BookAdapter(
    private var bookList: MutableList<Book>
) : RecyclerView.Adapter<BookAdapter.BookViewHolder>() {
    companion object {
        val bookImageOption = RequestOptions().format(DecodeFormat.PREFER_RGB_565)
    }

    var onItemClick: ((Book) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemBookBinding.inflate(inflater, parent, false)
        return BookViewHolder(binding)
    }

    fun setBookList(newList: MutableList<Book>) {
        try {
            val diffResult = DiffUtil.calculateDiff(BookDiffUtil(this.bookList, newList))
            this.bookList.clear()
            this.bookList.addAll(newList)
            diffResult.dispatchUpdatesTo(this)
        } catch (e: Exception) {
            Log.e(TAG, "#setCartItemList() error = ${e.message}")
            this.bookList.clear()
            this.bookList.addAll(newList)
        }
    }

    override fun getItemCount(): Int {
        return bookList.size
    }

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        holder.bind(bookList[position])
    }

    inner class BookViewHolder(private val binding: ItemBookBinding) :
        BaseViewHolder<Book>(binding.root) {

        override fun bind(dataItem: Book) {
            binding.itemBookName.text = dataItem.bookName
            binding.itemBookPrice.text = BookUtil.currencyFormat(dataItem.price)
            Glide.with(binding.itemBookImage)
                .load(dataItem.imageUrl)
                //.diskCacheStrategy(DiskCacheStrategy.NONE)
                //.skipMemoryCache(true)
                .fitCenter()
                .placeholder(R.drawable.ic_crop_original)
                .apply(bookImageOption)
                .into(binding.itemBookImage)
            itemView.setOnClickListener {
                onItemClick?.invoke(bookList[absoluteAdapterPosition])
            }
        }
    }
}