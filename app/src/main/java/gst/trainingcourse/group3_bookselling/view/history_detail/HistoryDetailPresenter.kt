package gst.trainingcourse.group3_bookselling.view.history_detail

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.base.BaseExecutor
import gst.trainingcourse.group3_bookselling.repository.entity.HistoryItem
import gst.trainingcourse.group3_bookselling.repository.source.HistoryRepository
import gst.trainingcourse.group3_bookselling.util.Constants
import kotlinx.coroutines.*

class HistoryDetailPresenter : BaseExecutor(), HistoryDetailContract.Presenter {
    private val historyItemLiveData = MutableLiveData<MutableList<HistoryItem>>()
    private val historyRepo = HistoryRepository()

    override fun observe(
        owner: LifecycleOwner,
        historyItemObserver: Observer<MutableList<HistoryItem>>,
        msgObserver: Observer<String>
    ) {
        historyItemLiveData.observe(owner, historyItemObserver)
        msgLiveData.observe(owner, msgObserver)
    }


    override fun loadHistoryItems(scope: CoroutineScope, orderId: String) {
        executeIO(scope) {
            val historyItems = historyRepo.getHistoryItems(orderId)
            withContext(Dispatchers.Main) {
                historyItemLiveData.value = historyItems
            }
        }
    }
}