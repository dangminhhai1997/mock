package gst.trainingcourse.group3_bookselling.view.paying

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import gst.trainingcourse.group3_bookselling.R
import gst.trainingcourse.group3_bookselling.base.BaseFragment

import gst.trainingcourse.group3_bookselling.view.cart.adapter.CartItemAdapter
import gst.trainingcourse.group3_bookselling.repository.entity.CartItem
import gst.trainingcourse.group3_bookselling.repository.entity.UserInfo
import gst.trainingcourse.group3_bookselling.databinding.FragmentPayingBinding
import gst.trainingcourse.group3_bookselling.view.home.HomeActivity
import gst.trainingcourse.group3_bookselling.util.BookUtil
import gst.trainingcourse.group3_bookselling.view.cart.CartFragment

private const val CART_DATA_KEY = "Data"
//private val TAG = ConfirmFragment::class.java.simpleName

class PayingFragment : BaseFragment(), PayingContract.View {
    private var binding: FragmentPayingBinding? = null
    private var presenter: PayingPresenter = PayingPresenter()
    private lateinit var adapter: CartItemAdapter
    private var fee = 0
    private var total = 0
    private lateinit var cartItems: MutableList<CartItem>

    private val interactObserver = Observer<Int> { command ->
        when (command) {
            COMMAND_CONFIRM -> showConfirmDialog()
            COMMAND_SUCCESS -> showSuccess()
        }
        hideLoadingView()
    }
    private val userObserver = Observer<UserInfo> { userInfo ->
        updatePayInfo(userInfo)
    }

    companion object {
        const val COMMAND_CONFIRM = 0
        const val COMMAND_SUCCESS = 1

        @JvmStatic
        fun newInstance(cartItemList: MutableList<CartItem>) = PayingFragment()
            .apply {
                arguments = Bundle().apply {
                    val cartItemData = ArrayList<CartItem>()
                    cartItemData.addAll(cartItemList)
                    putParcelableArrayList(CART_DATA_KEY, cartItemData)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.apply {
            cartItems = getParcelableArrayList(CART_DATA_KEY) ?: ArrayList()
            total = BookUtil.calculateTotal(cartItems)
            adapter = CartItemAdapter(cartItems, null)
        }
        presenter.observe(this, msgObserver, interactObserver, userObserver)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as? HomeActivity)?.hideBottom()
        binding = FragmentPayingBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun initView() {
        binding?.apply {
            payListBook.adapter = adapter
            payListBook.addItemDecoration(
                DividerItemDecoration(this@PayingFragment.context, LinearLayoutManager.VERTICAL)
            )
            //total with default is normal delivery form
            payTotalText.text =
                BookUtil.currencyFormat(total + resources.getInteger(R.integer.delivery_normal_fee))
            //confirm click event
            payOrderBtn.setOnClickListener {
                if (clickable) {
                    binding?.let {
                        showLoadingView()
                        presenter.confirm(this)
                    }
                }
            }
            //delivery form radio button event
            payDetail.deliveryFormFast.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    fee = resources.getInteger(R.integer.delivery_fast_fee)
                    payTotalText.text = BookUtil.currencyFormat(total + fee)
                }
            }
            payDetail.deliveryFormNormal.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    fee = resources.getInteger(R.integer.delivery_normal_fee)
                    payTotalText.text = BookUtil.currencyFormat(total + fee)
                }
            }
        }
    }

    override fun onDestroyView() {
        //show activity's bottom navigation
        (activity as? HomeActivity)?.showBottom()
        binding = null
        super.onDestroyView()
    }

    override fun updatePayInfo(userInfo: UserInfo) {
        binding?.payDetail?.apply {
            payAddress.setText(userInfo.address)
            payPhoneNumber.setText(userInfo.phone)
        }
    }

    override fun showLoadingView() {
        clickable = false
        binding?.payLoading?.show()
    }

    override fun hideLoadingView() {
        binding?.payLoading?.hide()
        clickable = true
    }

    /**
     * show a dialog to conform order, this dialog is not cancelable
     * ok to send order
     */
    override fun showConfirmDialog() {
        context?.run {
            AlertDialog.Builder(this).apply {
                setTitle(R.string.confirm)
                setMessage(R.string.confirm_order_msg)
                setNegativeButton(R.string.cancel) { dialog, _ ->
                    dialog.dismiss()
                }
                setPositiveButton(R.string.ok) { dialog, _ ->
                    showLoadingView()
                    presenter.sendOrder(lifecycleScope, cartItems, total + fee)
                    dialog.dismiss()
                }
                setCancelable(false)

            }.create().show()
        }
    }

    /**
     * Display the order success message, thí dialog is not cancelable
     * set ok as neutral button, back to cart fragment
     */
    override fun showSuccess() {
        context?.run {
            AlertDialog.Builder(this)
                .apply {
                    setTitle(R.string.notification)
                    setMessage(R.string.order_success)
                    setNeutralButton(R.string.ok) { dialog, _ ->
                        dialog.dismiss()
                    }
                    setOnDismissListener {
                        (activity as? HomeActivity)?.replaceFragment(CartFragment.newInstance())
                    }
                    setCancelable(false)
                }.create().show()
        }
    }
}
