package gst.trainingcourse.group3_bookselling.view.cart

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import gst.trainingcourse.group3_bookselling.base.BaseExecutor
import gst.trainingcourse.group3_bookselling.repository.entity.CartItem
import gst.trainingcourse.group3_bookselling.repository.source.CartRepository
import gst.trainingcourse.group3_bookselling.view.home.HomeActivity
import gst.trainingcourse.group3_bookselling.util.BookUtil
import gst.trainingcourse.group3_bookselling.view.paying.PayingFragment
import kotlinx.coroutines.*

class CartPresenter : BaseExecutor(), CartContract.Presenter {
    private var isExecutingUpdate = false
    private val cartLiveData = MutableLiveData<MutableList<CartItem>>()
    private val orderLiveData = MutableLiveData<String>()
    private val cartRepo = CartRepository()

    override fun observe(
        owner: LifecycleOwner,
        cartObserver: Observer<MutableList<CartItem>>,
        orderObserver: Observer<String>,
        msgObserver: Observer<String>
    ) {
        cartLiveData.observe(owner, cartObserver)
        orderLiveData.observe(owner, orderObserver)
        msgLiveData.observe(owner, msgObserver)
    }

    override fun calculateTotal() {
        val list = cartLiveData.value
        list?.run {
            orderLiveData.value = BookUtil.currencyFormat(BookUtil.calculateTotal(this))
        }
    }

    override fun removeItem(scope: CoroutineScope, cartItemId: String) {
        updateItemByQuantity(scope, cartItemId, 0)
    }

    override fun updateQuantity(scope: CoroutineScope, quantity: Int, cartItemId: String) {
        updateItemByQuantity(scope, cartItemId, quantity)
    }

    override fun loadCart(scope: CoroutineScope) {
        executeIO(scope) {
            val cartItems = cartRepo.getCartItems()
            withContext(Dispatchers.Main) {
                cartLiveData.value = cartItems
            }
        }
    }

    override fun confirmOrder(activity: FragmentActivity?) {
        (activity as? HomeActivity)?.run {
            cartLiveData.value?.let {
                replaceFragment(PayingFragment.newInstance(it))
            }
        }
    }

    /**
     * update cart items
     * only 1 update action can be performed at a time
     * remove item if quantity <= 0 else update its quantity
     * @param scope -> to launch update action
     * @param itemId -> id of cart item to update
     * @param quantity -> quantity to update
     */
    private fun updateItemByQuantity(scope: CoroutineScope, itemId: String, quantity: Int) {
        if (isExecutingUpdate) return else isExecutingUpdate = true
        executeIO(scope) {
            //get current list of CartItem
            val list = cartLiveData.value
            //check if it is null or empty
            if (list != null && list.isNotEmpty()) {
                //find the position of the book to update
                val updatePos = BookUtil.findCartItemPos(list, itemId)
                if (updatePos != null) {
                    //copy item? if not, remove does not work?
                    val newItem = list[updatePos].copy()
                    newItem.quantity = quantity
                    if (quantity <= 0) {//remove item if quantity <= 0
                        if (cartRepo.removeCartItem(newItem)) {
                            list.removeAt(updatePos)
                            withContext(Dispatchers.Main) { notifyData(list) }
                        }
                    } else {//update item if quantity > 0
                        if (cartRepo.updateCartItemQuantity(newItem)) {
                            list[updatePos] = newItem
                            withContext(Dispatchers.Main) { notifyData(list) }
                        }
                    }
                }
                isExecutingUpdate = false
            }
        }
    }

    private fun notifyData(cartItems: MutableList<CartItem>) {
        val total = BookUtil.currencyFormat(BookUtil.calculateTotal(cartItems))
        cartLiveData.value = cartItems
        orderLiveData.value = total
    }
}