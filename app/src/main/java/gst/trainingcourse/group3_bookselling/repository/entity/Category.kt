package gst.trainingcourse.group3_bookselling.repository.entity

data class Category(val id: Int, val name: String)