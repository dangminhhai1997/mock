package gst.trainingcourse.group3_bookselling.repository.source

import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import gst.trainingcourse.group3_bookselling.repository.entity.Book
import gst.trainingcourse.group3_bookselling.repository.entity.CartItem
import gst.trainingcourse.group3_bookselling.util.Constants
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import kotlin.collections.ArrayList

//private val TAG = CartRepository::class.java.simpleName

/**
 * all method use Task with timeout and may:
 * @throws InterruptedException
 * @throws ExecutionException
 * @throws TimeoutException
 */
class CartRepository {
    fun getCartItems(): MutableList<CartItem> {
        val email = FirebaseAuth.getInstance().currentUser?.email
        val listResult: MutableList<CartItem> = ArrayList()
        //get reference to cart collection of given userId
        val cartRef = Firebase.firestore
            .document("${Constants.COLLECTION_USER}/$email")
            .collection(Constants.COLLECTION_CART)
        //get data
        val task = cartRef.get()
        val taskResult = Tasks.await(task, Constants.BASE_TIME_OUT, TimeUnit.MILLISECONDS)
        if (task.isSuccessful) {
            var cartItem: CartItem
            for (document in taskResult) {
                cartItem = CartItem()
                with(cartItem) {
                    id = document.id
                    quantity = document.data[Constants.BOOK_QUANTITY].toString().toInt()
                    book.bookName = document.data[Constants.BOOK_NAME].toString()
                    book.price = document.data[Constants.BOOK_PRICE].toString().toInt()
                    book.imageUrl = document.data[Constants.BOOK_IMAGE_URL].toString()
                }
                listResult.add(cartItem)
            }
        }
        return listResult
    }

    /**
     * update cart item quantity by userId and CartItem
     */
    fun updateCartItemQuantity(item: CartItem): Boolean {
        val email = FirebaseAuth.getInstance().currentUser?.email
        val itemRef = Firebase.firestore
            .document("${Constants.COLLECTION_USER}/$email")
            .collection(Constants.COLLECTION_CART)
            .document(item.id)
        return updateCartItemQuantity(itemRef, item.quantity)
    }

    /**
     * update cart item quantity by reference on firestore
     */
    private fun updateCartItemQuantity(ref: DocumentReference, quantity: Int): Boolean {
        val task = ref.update(Constants.BOOK_QUANTITY, quantity)
        Tasks.await(task, Constants.BASE_TIME_OUT, TimeUnit.MILLISECONDS)
        return task.isSuccessful
    }

    fun removeCartItem(item: CartItem): Boolean {
        val email = FirebaseAuth.getInstance().currentUser?.email
        val itemRef = Firebase.firestore
            .document("${Constants.COLLECTION_USER}/$email")
            .collection(Constants.COLLECTION_CART)
            .document(item.id)
        val task = itemRef.delete()
        Tasks.await(task, Constants.BASE_TIME_OUT, TimeUnit.MILLISECONDS)
        return task.isSuccessful
    }

    /**
     * add a book to user's cart
     * if the book is already exist, update first item in checkResult, inc its quantity by 1
     * else update its quantity += 1
     *
     * @param book -> book's info
     * @return -> success or not
     */
    fun addToCart(book: Book): Boolean {
        val email = FirebaseAuth.getInstance().currentUser?.email
        //reference to user's cart collection
        val itemRef = Firebase.firestore
            .document("${Constants.COLLECTION_USER}/$email")
            .collection(Constants.COLLECTION_CART)
        //check if this book is already exist in cart
        val checkExistTask = itemRef.whereEqualTo(Constants.BOOK_ID, book.bookId).get()
        val checkResult =
            Tasks.await(checkExistTask, Constants.BASE_TIME_OUT, TimeUnit.MILLISECONDS)
        //if the book is not exist, add it to cart with quantity = 1
        if (checkResult.isEmpty) {
            val data = hashMapOf(
                Constants.BOOK_ID to book.bookId,
                Constants.BOOK_NAME to book.bookName,
                Constants.BOOK_IMAGE_URL to book.imageUrl,
                Constants.BOOK_PRICE to book.price,
                Constants.BOOK_QUANTITY to 1
            )
            return addItem(data, itemRef)
        } else {
            //get current quantity of first item
            val currentQuantity =
                checkResult.documents[0].get(Constants.BOOK_QUANTITY).toString().toInt()
            //get the id of document
            val cartItemId = checkResult.documents[0].id
            //update
            return updateCartItemQuantity(itemRef.document(cartItemId), currentQuantity + 1)
        }
    }

    /**
     * add data by its reference
     */
    private fun addItem(data: Any, ref: CollectionReference): Boolean {
        val task = ref.add(data)
        Tasks.await(task, Constants.BASE_TIME_OUT, TimeUnit.MILLISECONDS)
        return task.isSuccessful
    }

    fun addToHistory(cartItems: MutableList<CartItem>, totalValue: Int): Boolean {
        val userEmail = FirebaseAuth.getInstance().currentUser?.email
        //create new document in history collection
        val historyRef = Firebase.firestore
            .document("${Constants.COLLECTION_USER}/$userEmail")
            .collection(Constants.COLLECTION_HISTORY)
            .document()
        //put base data
        val dateString = SimpleDateFormat.getDateInstance(DateFormat.DATE_FIELD).format(Date())
        val historyBaseInfo = hashMapOf(
            Constants.HISTORY_DATE to dateString,
            Constants.HISTORY_VALUE to totalValue
        )
        Tasks.await(historyRef.set(historyBaseInfo), Constants.BASE_TIME_OUT, TimeUnit.MILLISECONDS)
        //update list of items in history
        val historyItemRef = historyRef.collection(Constants.HISTORY_ITEMS)
        //run a batch
        val addHistoryItemsTask = Firebase.firestore.runBatch {
            var itemDocRef: DocumentReference
            //with each item, create new document and set data
            for (item in cartItems) {
                val data = hashMapOf(
                    Constants.BOOK_NAME to item.book.bookName,
                    Constants.BOOK_QUANTITY to item.quantity,
                    Constants.BOOK_PRICE to item.book.price
                )
                itemDocRef = historyItemRef.document()
                it.set(itemDocRef, data)
            }
        }
        Tasks.await(addHistoryItemsTask, Constants.BASE_TIME_OUT, TimeUnit.MILLISECONDS)
        return addHistoryItemsTask.isSuccessful
    }

    fun clearCart(): Boolean {
        val userEmail = FirebaseAuth.getInstance().currentUser?.email
        //get all document in cart of user
        val ref = Firebase.firestore
            .document("${Constants.COLLECTION_USER}/$userEmail")
            .collection(Constants.COLLECTION_CART)
        val getCartItemTask = ref.get()
        val taskResult =
            Tasks.await(getCartItemTask, Constants.BASE_TIME_OUT, TimeUnit.MILLISECONDS)
        //delete all
        return if (getCartItemTask.isSuccessful) {
            val deleteTask = Firebase.firestore.runBatch {
                var cartItemRef: DocumentReference
                for (item in taskResult.documents) {
                    cartItemRef = ref.document(item.id)
                    it.delete(cartItemRef)
                }
            }
            Tasks.await(deleteTask, Constants.BASE_TIME_OUT, TimeUnit.MILLISECONDS)
            deleteTask.isSuccessful
        } else {
            false
        }
    }
}