package gst.trainingcourse.group3_bookselling.repository.entity

import android.os.Parcel
import android.os.Parcelable

data class Book(
    //book id is document id on fire store
    var bookId: String = "",
    var bookName: String = "",
    var imageUrl: String = "",
    var price: Int = 0,
    var author: String = "",
    var publishYear: Int = 0,
    var publisher: String = "",
    var category: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readInt(),
        parcel.readString() ?: "",
        parcel.readInt(),
        parcel.readString() ?: "",
        parcel.readString() ?: ""
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(bookId)
        parcel.writeString(bookName)
        parcel.writeString(imageUrl)
        parcel.writeInt(price)
        parcel.writeString(author)
        parcel.writeInt(publishYear)
        parcel.writeString(publisher)
        parcel.writeString(category)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Book> {
        override fun createFromParcel(parcel: Parcel): Book {
            return Book(parcel)
        }

        override fun newArray(size: Int): Array<Book?> {
            return arrayOfNulls(size)
        }
    }


}