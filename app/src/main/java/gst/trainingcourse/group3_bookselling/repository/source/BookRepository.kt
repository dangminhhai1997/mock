package gst.trainingcourse.group3_bookselling.repository.source

import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import gst.trainingcourse.group3_bookselling.repository.entity.Book
import gst.trainingcourse.group3_bookselling.util.Constants
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

//private val TAG = BookRepository::class.java.simpleName

/**
 * all method use Task with timeout and may:
 * @throws InterruptedException
 * @throws ExecutionException
 * @throws TimeoutException
 */
class BookRepository {

    fun getByCategory(id: Int): MutableList<Book> {
        val db = Firebase.firestore
        val task = db.collection(Constants.COLLECTION_BOOK)
            .whereEqualTo(Constants.BOOK_CATEGORY, Constants.categoryArray[id].name)
            .limit(20)
            .get()
        return parseResult(task)
    }

    fun searchByName(name: String): MutableList<Book> {
        val task = Firebase.firestore
            .collection(Constants.COLLECTION_BOOK)
            .whereGreaterThanOrEqualTo(Constants.BOOK_NAME, name)
            .limit(20)
            .get()
        return parseResult(task)
    }

    fun searchByAuthor(author: String): MutableList<Book> {
        val task = Firebase.firestore
            .collection(Constants.COLLECTION_BOOK)
            .whereGreaterThanOrEqualTo(Constants.BOOK_AUTHOR, author)
            .limit(20)
            .get()
        return parseResult(task)
    }

    private fun parseResult(task: Task<QuerySnapshot>): MutableList<Book> {
        val resultList: MutableList<Book> = ArrayList()
        //wait for task to get result
        val snapshot = Tasks.await(task, Constants.BASE_TIME_OUT, TimeUnit.MILLISECONDS)
        var bookItem: Book
        if (task.isSuccessful) {
            for (document in snapshot.documents) {
                bookItem = Book()
                bookItem.bookId = document.id
                bookItem.bookName = document.get(Constants.BOOK_NAME).toString()
                bookItem.author = document.get(Constants.BOOK_AUTHOR).toString()
                bookItem.category = document.get(Constants.BOOK_CATEGORY).toString()
                bookItem.imageUrl = document.get(Constants.BOOK_IMAGE_URL).toString()
                bookItem.publisher = document.get(Constants.BOOK_PUBLISHER).toString()
                bookItem.publishYear = document.get(Constants.BOOK_PUBLISH_YEAR).toString().toInt()
                bookItem.price = document.get(Constants.BOOK_PRICE).toString().toInt()
                resultList.add(bookItem)
            }
        }
        return resultList
    }
}