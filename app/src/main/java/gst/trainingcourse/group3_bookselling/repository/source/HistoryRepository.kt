package gst.trainingcourse.group3_bookselling.repository.source

import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import gst.trainingcourse.group3_bookselling.repository.entity.HistoryItem
import gst.trainingcourse.group3_bookselling.repository.entity.OrderHistory
import gst.trainingcourse.group3_bookselling.util.Constants
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeoutException

//private val TAG = HistoryRepository::class.java.simpleName

/**
 * all method use Task with timeout and may:
 * @throws InterruptedException
 * @throws ExecutionException
 * @throws TimeoutException
 */
class HistoryRepository {

    fun getHistory(): MutableList<OrderHistory> {
        val historyList: MutableList<OrderHistory> = ArrayList()
        val email = FirebaseAuth.getInstance().currentUser?.email
        val userHistoryRef = Firebase.firestore
            .document("${Constants.COLLECTION_USER}/$email")
            .collection(Constants.COLLECTION_HISTORY)
        //get all history of this user
        val taskResult = Tasks.await(userHistoryRef.get())
        var orderHistory: OrderHistory
        //with each history result
        for (document in taskResult) {
            orderHistory = OrderHistory()
            orderHistory.historyId = document.id
            orderHistory.date = document.get(Constants.HISTORY_DATE).toString()
            orderHistory.value = document.get(Constants.HISTORY_VALUE).toString().toInt()
            historyList.add(orderHistory)
        }
        return historyList
    }

    fun getHistoryItems(historyId: String): MutableList<HistoryItem> {
        val email = FirebaseAuth.getInstance().currentUser?.email
        //refer to current user history collection, them history item document,then items collection
        val historyItemRef = Firebase.firestore
            .document("${Constants.COLLECTION_USER}/$email")
            .collection(Constants.COLLECTION_HISTORY)
            .document(historyId)
            .collection(Constants.COLLECTION_ITEM)
        //
        val itemList: MutableList<HistoryItem> = ArrayList()
        val itemResult = Tasks.await(historyItemRef.get())
        var item: HistoryItem
        for (result in itemResult) {
            item = HistoryItem()
            item.bookName = result.get(Constants.BOOK_NAME).toString()
            item.price = result.get(Constants.BOOK_PRICE).toString().toInt()
            item.quantity = result.get(Constants.BOOK_QUANTITY).toString().toInt()
            itemList.add(item)
        }
        return itemList
    }
}