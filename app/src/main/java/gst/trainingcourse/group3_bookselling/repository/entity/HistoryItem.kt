package gst.trainingcourse.group3_bookselling.repository.entity

class HistoryItem(
    var bookName: String = "",
    var price: Int = 0,
    var quantity: Int = 0
)