package gst.trainingcourse.group3_bookselling.repository.entity

import android.os.Parcel
import android.os.Parcelable

class OrderHistory(
    var date: String = "",
    var historyId: String = "",
    var value: Int = 0,
    var status: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readInt(),
        parcel.readString() ?: ""
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(date)
        parcel.writeString(historyId)
        parcel.writeInt(value)
        parcel.writeString(status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OrderHistory> {
        override fun createFromParcel(parcel: Parcel): OrderHistory {
            return OrderHistory(parcel)
        }

        override fun newArray(size: Int): Array<OrderHistory?> {
            return arrayOfNulls(size)
        }
    }
}