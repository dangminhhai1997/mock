package gst.trainingcourse.group3_bookselling.repository.source

import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import gst.trainingcourse.group3_bookselling.repository.entity.UserInfo
import gst.trainingcourse.group3_bookselling.util.Constants
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

//private val TAG = UserRepository::class.java.simpleName

/**
 * all method use Task with timeout and may:
 * @throws InterruptedException
 * @throws ExecutionException
 * @throws TimeoutException
 */
class UserRepository {

    fun isLoggedIn(): Boolean {
        val user = FirebaseAuth.getInstance().currentUser
        return user != null
    }

    fun logOut() {
        val auth = FirebaseAuth.getInstance()
        auth.signOut()
    }

    /**
     * get current user info from fire store
     * first, check if user is logged in using firebase auth and get email
     * if not return null
     * else try
     *
     * @return UserInfo or null if user is not logged in, or get info from fire store fail
     */
    fun getCurrentUserInfo(): UserInfo? {
        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser != null) {
            //reference to current user in fire store
            val userRef = Firebase.firestore
                .document("${Constants.COLLECTION_USER}/${currentUser.email}")
            //get info
            val getUserInfoTask = userRef.get()
            val result =
                Tasks.await(getUserInfoTask, Constants.BASE_TIME_OUT, TimeUnit.MILLISECONDS)
            if (getUserInfoTask.isSuccessful) {
                //parse result
                val userInfo = UserInfo()
                userInfo.email = result.id
                userInfo.name = result.get(Constants.USER_NAME).toString()
                userInfo.phone = result.get(Constants.USER_PHONE).toString()
                userInfo.address = result.get(Constants.USER_ADDRESS).toString()
                userInfo.birth = result.get(Constants.USER_BIRTH).toString()
                userInfo.gender = result.get(Constants.USER_GENDER).toString()
                return userInfo
            }
        }
        return null
    }

    /**
     * check if this user is already exist in fire store
     * @param email -> user's email
     * @return true if user is exist else false
     */
    fun checkUserExist(email: String): Boolean {
        val userRef = Firebase.firestore
            .document("${Constants.COLLECTION_USER}/$email")
        val result = Tasks.await(userRef.get(), Constants.BASE_TIME_OUT, TimeUnit.MILLISECONDS)
        return result.exists()
    }

    fun logInWithEmail(email: String, password: String): Boolean {
        println("login")
        val db = FirebaseAuth.getInstance()
        return try {
            val task = db.signInWithEmailAndPassword(email, password)
            Tasks.await(task, Constants.BASE_TIME_OUT, TimeUnit.MILLISECONDS)
            task.isSuccessful
        } catch (e: ExecutionException) {
            false
        }

    }

    /**
     * try create user with firebase auth
     * then add a document with id = email
     * then create empty info
     *
     * @param email -> email address
     * @param password -> password
     *
     * @return -> true if success else false
     */
    fun signUpWithEmail(email: String, password: String): Boolean {
        val db = FirebaseAuth.getInstance()
        val task = db.createUserWithEmailAndPassword(email, password)
        Tasks.await(task, Constants.BASE_TIME_OUT, TimeUnit.MILLISECONDS)
        if (task.isSuccessful) {
            //add empty info to fire store
            val userInfo = UserInfo()
            userInfo.email = email
            updateUserInfo(userInfo)
            //don't care if add user info is successful or not
        }
        return task.isSuccessful
    }

    fun updateUserInfo(userInfo: UserInfo): Boolean {
        val userRef = Firebase.firestore
            .document("${Constants.COLLECTION_USER}/${userInfo.email}")
        //put data except email
        val data = hashMapOf(
            Constants.USER_PHONE to userInfo.phone,
            Constants.USER_ADDRESS to userInfo.address,
            Constants.USER_NAME to userInfo.name,
            Constants.USER_BIRTH to userInfo.birth,
            Constants.USER_GENDER to userInfo.gender
        )
        val task = userRef.set(data)
        Tasks.await(task, Constants.BASE_TIME_OUT, TimeUnit.MILLISECONDS)
        return task.isSuccessful
    }
}