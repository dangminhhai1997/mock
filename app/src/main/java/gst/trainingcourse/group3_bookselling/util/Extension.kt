package gst.trainingcourse.group3_bookselling.util

import android.util.Patterns

fun String.isValidEmail(): Boolean {
    return Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun String.isValidPassword(): Boolean {
    return UserUtil.REGEX_PASSWORD.toRegex().matches(this)
}