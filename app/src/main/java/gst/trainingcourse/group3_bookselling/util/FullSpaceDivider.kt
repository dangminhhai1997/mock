package gst.trainingcourse.group3_bookselling.util

import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView

class FullSpaceDivider(private val space: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, itemPosition: Int, parent: RecyclerView) {
        outRect.set(space, space, space, space)
    }
}