package gst.trainingcourse.group3_bookselling.util

import gst.trainingcourse.group3_bookselling.repository.entity.Category

object Constants {
    const val BASE_TIME_OUT: Long = 15000

    //db collections
    const val COLLECTION_BOOK = "Book"
    const val COLLECTION_USER = "User"
    const val COLLECTION_CART = "cart"
    const val COLLECTION_HISTORY = "history"
    const val COLLECTION_ITEM = "items"

    //book field
    const val BOOK_ID = "bookId"
    const val BOOK_NAME = "bookName"
    const val BOOK_IMAGE_URL = "imageUrl"
    const val BOOK_QUANTITY = "quantity"
    const val BOOK_PRICE = "price"
    const val BOOK_AUTHOR = "author"
    const val BOOK_CATEGORY = "category"
    const val BOOK_PUBLISHER = "publisher"
    const val BOOK_PUBLISH_YEAR = "publishYear"

    //history field
    const val HISTORY_DATE = "date"
    const val HISTORY_VALUE = "value"
    const val HISTORY_ITEMS = "items"

    //user field
    const val USER_BIRTH = "birth"
    const val USER_ADDRESS = "address"
    const val USER_GENDER = "gender"
    const val USER_PHONE = "phone"
    const val USER_NAME = "name"
    const val USER_NAME_UNDEFINED = "Unknown User"
    const val USER_INFO_UNDEFINED = "Unknown"
    const val USER_GENDER_MALE = "Male"
    const val USER_GENDER_FEMALE = "Female"

    //msg
    const val MSG_EMPTY = "Please fill info!"
    const val MSG_ERROR = "Something went wrong, please try again later!"
    const val MSG_NETWORK_OFF = "No internet connection!"
    const val MSG_ADD_CART_SUCCESS = "Add to cart successfully!"
    const val MSG_ADD_CART_FAILED = "Add to cart failed, please try again later!"
    const val MSG_UPDATE_INFO_FAILED = "Update info failed, please try again later!"
    const val MSG_UPDATE_INFO_SUCCESS = "Update info successfully!"
    const val MSG_LOG_IN_SUCCESS = "Sign in successfully!"
    const val MSG_SIGN_UP_SUCCESS = "Sign up successfully!"
    const val MSG_LOG_IN_FAIL = "Login failed! Check your email and password or try again later!"
    const val MSG_EMAIL_INVALID = "Invalid email!"
    const val MSG_EMAIL_EXIST = "This email already exists!"
    const val MSG_PASSWORD_INVALID =
        "Password must contain at least 8 characters, a number and uppercase letter!"

    val categoryArray =
        arrayOf(
            Category(0, "Comic - Manga"),
            Category(1, "Science"),
            Category(2, "Computer Science"),
            Category(3, "Literary"),
            Category(4, "Economy"),
            Category(5, "Cultural - Geography - Travel"),
            Category(6, "History")
        )
}