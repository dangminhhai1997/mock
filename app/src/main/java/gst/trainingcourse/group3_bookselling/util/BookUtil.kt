package gst.trainingcourse.group3_bookselling.util

import gst.trainingcourse.group3_bookselling.repository.entity.CartItem
import java.text.NumberFormat

object BookUtil {
    fun calculateTotal(list: MutableList<CartItem>): Int {
        var result = 0
        list.forEach {
            result += it.quantity * it.book.price
        }
        return result
    }

    fun currencyFormat(amount: Int): String {
        val numberFormat = NumberFormat.getCurrencyInstance()
        return numberFormat.format(amount)
    }

    fun findCartItemPos(listCartItem: MutableList<CartItem>, itemId: String): Int? {
        for (i in listCartItem.indices) {
            if (listCartItem[i].id == itemId) {
                return i
            }
        }
        return null
    }
}