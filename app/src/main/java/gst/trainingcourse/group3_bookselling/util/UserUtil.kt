package gst.trainingcourse.group3_bookselling.util

import java.lang.StringBuilder

object UserUtil {
    const val REGEX_PASSWORD = "(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z]).{8,16}"
    fun formatBirth(year: Int, month: Int, day: Int): String {
        return "$year/$month/$day"
    }
}