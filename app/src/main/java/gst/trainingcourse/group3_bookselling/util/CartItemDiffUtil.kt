package gst.trainingcourse.group3_bookselling.util

import androidx.recyclerview.widget.DiffUtil
import gst.trainingcourse.group3_bookselling.repository.entity.CartItem

class CartItemDiffUtil(
    private val oldList: MutableList<CartItem>,
    private val newList: MutableList<CartItem>
) :
    DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]
        return oldItem.id == newItem.id
    }

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]
        return oldItem.id == newItem.id
                && oldItem.book.bookId == newItem.book.bookId
                && oldItem.quantity == newItem.quantity
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        return newList[newItemPosition].quantity
    }
}