package gst.trainingcourse.group3_bookselling.util

import androidx.recyclerview.widget.DiffUtil
import gst.trainingcourse.group3_bookselling.repository.entity.Book

class BookDiffUtil(
    private val oldList: MutableList<Book>,
    private val newList: MutableList<Book>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].bookId == newList[newItemPosition].bookId
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]
        return oldItem.bookId == newItem.bookId
                && oldItem.bookName == newItem.bookName
                && oldItem.author == newItem.author
                && oldItem.category == newItem.category
    }
}